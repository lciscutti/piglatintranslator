package piglatintranslator;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals(inputPhrase, translator.getPhrase());
	}
	
	@Test
	public void testTranslationNullInputPhrase() throws PigLatinException {
		String inputPhrase = "";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals(Translator.NIL, translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithAAndEndigWithY() throws PigLatinException {
		String inputPhrase = "any";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("anynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithUAndEndigWithY() throws PigLatinException {
		String inputPhrase = "utility";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("utilitynay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelAndEndigWithVowel() throws PigLatinException {
		String inputPhrase = "apple";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("appleyay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithVowelAndEndigWithConsonant() throws PigLatinException {
		String inputPhrase = "ask";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("askay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithSingleConsonant() throws PigLatinException {
		String inputPhrase = "hello";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreConsonant() throws PigLatinException {
		String inputPhrase = "known";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ownknay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreWordsBySpace() throws PigLatinException {
		String inputPhrase = "hello world";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreWordsByScore() throws PigLatinException {
		String inputPhrase = "well-being";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellway-eingbay", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseStartingWithMoreWordsByScoreAndSpace() throws PigLatinException {
		String inputPhrase = "well-being hello";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellway-eingbay ellohay", translator.translate());
	}

	@Test
	public void testTranslationPhraseContainingPunctuation() throws PigLatinException {
		String inputPhrase = "hello world!";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay orldway!", translator.translate());
	}
	
	@Test
	public void testTranslationPhraseContainingPunctuations() throws PigLatinException {
		String inputPhrase = "hello, world!";
		Translator translator = new Translator(inputPhrase);
		
		assertEquals("ellohay, orldway!", translator.translate());
	}


}
