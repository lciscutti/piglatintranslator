package piglatintranslator;

public class PigLatinException extends Exception {

	private static final long serialVersionUID = 1L;
	private static String errorMessage= "Frase input non valida a livello di punteggiatura!";
	public PigLatinException() {
        super(errorMessage);
        System.out.print(errorMessage);
    }
	
}
