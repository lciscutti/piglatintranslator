package piglatintranslator;

public class Translator {
private String phrase;
public static final String NIL = "nil";

public Translator(String phrase) {
	this.phrase = phrase;
}

public String getPhrase() {
	return phrase;
}

public String translate() throws PigLatinException {
	String[] words = phrase.split("[ \\-]");
	StringBuilder editedPhrase = new StringBuilder("");
	String divisor = "";
	
	if (phrase.contains("-") || phrase.contains(" ")) divisor = returnSpacesAndScores(phrase);
	
	if(!phrase.equals("")) {
		
		for(int i=0; i<=words.length-1; i++) {
			
			if(startWithVowel(words[i])) {
				editedPhrase.replace(0,editedPhrase.length() ,startWithVowelTranslation(words[i],editedPhrase.toString()));
			}else if(startWithConsonant(words[i])) {
				editedPhrase.append(returnAlteredWordWithoutMovePunctuations(moveFirstConsonantsToLast(words[i]), "ay"));
			}else if(startWithPuntuations(words[i])) throw new PigLatinException();
			if(i<words.length-1 && words.length>0) editedPhrase.append(divisor.charAt(i));
		}
		return editedPhrase.toString();
	}

	return NIL;
}

private String startWithVowelTranslation(String word, String editedPhrase) {
	if(word.endsWith("y")) {
		return editedPhrase + returnAlteredWordWithoutMovePunctuations(word, "nay");
	}else if(endWithVowel(word)) {
		return editedPhrase + returnAlteredWordWithoutMovePunctuations(word, "yay");
	}else return editedPhrase + returnAlteredWordWithoutMovePunctuations(word, "ay");
}

private boolean startWithVowel(String phrase) {
	return phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u");
}

private boolean endWithVowel(String phrase) {
	return phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u");
}

private boolean startWithConsonant(String phrase) {
	switch(phrase.charAt(0)) {
	case 'b': return true; 
	case 'c': return true;
	case 'd': return true;
	case 'f': return true;
	case 'g': return true;
	case 'h': return true;
	case 'k': return true;
	case 'l': return true;
	case 'm': return true;
	case 'n': return true;
	case 'p': return true;
	case 'q': return true;
	case 'r': return true;
	case 's': return true;
	case 't': return true;
	case 'v': return true;
	case 'w': return true;
	case 'x': return true;
	case 'y': return true;
	case 'z': return true;
	default: return false;
	}
}

private boolean startWithPuntuations(String phrase) {
	switch(phrase.charAt(0)) {
	case '.': return true; 
	case ',': return true;
	case ';': return true;
	case ':': return true;
	case '\'': return true;
	case '!': return true;
	case '?': return true;
	case '(': return true;
	case ')': return true;
	default: return false;
	}
}

private String  moveFirstConsonantsToLast(String phrase){
	String edited = phrase;
	for(int i=0; i<=phrase.length(); i++) {
		if (!startWithVowel(edited)) {
			edited = edited.substring(1)+edited.charAt(0);
		}
	}
	return edited;
}

private String returnSpacesAndScores(String phrase) {
	StringBuilder spacesAndScores = new StringBuilder("");
	for(int i=0; i<phrase.length(); i++) {
		if(phrase.charAt(i) == '-' || phrase.charAt(i) == ' ') {
			spacesAndScores.append(phrase.charAt(i));
		}
	}
	return spacesAndScores.toString();
}

private String returnAlteredWordWithoutMovePunctuations(String word, String suffix) {
	StringBuilder punctuations = new StringBuilder("");
	StringBuilder sb = new StringBuilder(word);
	for(int i=0; i<word.length(); i++) {
		if(word.charAt(i) == '.' || word.charAt(i) == ',' || word.charAt(i) == ':' || word.charAt(i) == ';' || word.charAt(i) == '?' || word.charAt(i) == '!' || word.charAt(i) == '(' || word.charAt(i) == ')' || word.charAt(i) == '\'') {
			punctuations.append(word.charAt(i));
			sb.deleteCharAt(i);
		}else punctuations.append(" ");
	}
	sb.append(suffix);
	
	for(int i=0; i<punctuations.length(); i++) {
		if(punctuations.charAt(i)!=' ') {
			sb.insert(i+suffix.length()+1, punctuations.charAt(i));
		}
	}
	return sb.toString();
}

}
